package tastat;

import java.util.Set;

public class Client{
	protected int idClient;
	protected String nomClient;
	protected String CIF;
	protected boolean actiu;
	protected String direccio;
	protected String poblacio;
	protected String pais;
	protected String personaContacte;
	protected String telefon;
	protected double latitud;
	protected double longitud;	
	Client(){
		idClient = Generador.getNextClient();
	} 
	
	
	Client(String nom){
		this();
		nomClient = nom;
	}
	
	Client (String nom, double lat, double lon){
		this(nom);
		latitud = lat;
		longitud = lon;
	}
}