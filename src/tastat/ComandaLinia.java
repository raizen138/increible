package tastat;

public class ComandaLinia {
	protected Producte producte;
	protected int quantitat;
	protected double preuVenda;
	
	ComandaLinia(Producte p, int q, double preu) {
		producte = p;
		quantitat = q;
		preuVenda = preu;
	}
}